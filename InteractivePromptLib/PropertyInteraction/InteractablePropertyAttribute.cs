﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

using InteractivePromptLib.Reflection;

namespace InteractivePromptLib
{
	/// <summary>
	/// Marks the member for use with <see cref="PropertyInteraction"/>.
	/// </summary>
	[AttributeUsage((AttributeTargets.Field | AttributeTargets.Property))]
	public class InteractablePropertyAttribute : Attribute
	{
		private MemberInfo member;

		/// <summary>
		/// The member this is attatched to.
		/// </summary>
		public MemberInfo Member => member;

		/// <summary>
		/// Name override, if null the name of the <see cref="Member"/> will be used.
		/// </summary>
		protected readonly string nameOVerride;

		/// <summary>
		/// Name of this item.
		/// </summary>
		public string Name => string.IsNullOrWhiteSpace(nameOVerride) ? member.Name : nameOVerride;



		/// <summary>
		/// Short, singe line help
		/// </summary>
		public readonly string OneLineHelp;

		/// <summary>
		/// Full help
		/// </summary>
		public readonly string LongHelp;

		/// <summary>
		/// Is the item disabled, this would make it not work.
		/// </summary>
		public readonly bool Disabled;

		/// <summary>
		/// Hide this item from lists.
		/// </summary>
		public readonly bool Hidden;

		/// <summary>
		/// Aliases of this item
		/// </summary>
		public readonly IReadOnlyList<string> Aliases;

		private bool isProperty = false;
		private bool isStatic = false;


		public bool IsProperty => isProperty;
		public bool IsField => !isProperty;

		public bool IsStatic => isStatic;
		public bool IsInstance => !isStatic;

		private Type valueType;

		/// <summary>
		/// The type of the value.
		/// </summary>
		public Type ValueType => valueType;

		/// <summary>
		/// Programmer signaled that Setting is not allowed
		/// </summary>
		public readonly bool BlockSetting;

		/// <summary>
		/// Programmer signaloed that Getting is not allowed.
		/// </summary>
		public readonly bool BlockGetting;

		private bool isSettable = false;
		private bool isGettable = false;

		/// <summary>
		/// Is Setting possible
		/// </summary>
		public bool IsSettable => isSettable;

		/// <summary>
		/// Is Getting possible
		/// </summary>
		public bool IsGettable => isGettable;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="nameOVerride"></param>
		/// <param name="oneLineHelp"></param>
		/// <param name="longHelp"></param>
		/// <param name="disabled"></param>
		/// <param name="hidden"></param>
		/// <param name="aliases"></param>
		/// <param name="blockGetting"></param>
		/// <param name="blockSetting"></param>
		public InteractablePropertyAttribute(
			string nameOVerride = null, 
			string oneLineHelp = null,
			string longHelp = null,
			bool disabled = false,
			bool hidden = false,
			string[] aliases = null,
			bool blockGetting = false,
			bool blockSetting = false)
		{
			this.nameOVerride = nameOVerride;
			this.OneLineHelp = oneLineHelp;
			this.LongHelp = longHelp;

			Disabled = disabled;
			Hidden = hidden;


			Aliases = null != aliases ? new ReadOnlyCollection<string>(aliases) :  null;

			this.BlockGetting = blockGetting;
			this.BlockSetting = blockSetting;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="member"></param>
		/// <throws>ReflectionException</throws>
		internal void SetMemberData(MemberInfo member)
		{
			this.member = member;

			var property = member as PropertyInfo;
			var field = member as FieldInfo;
			if (null != property)
			{
				isProperty = true;
				isStatic = property.GetAccessors().Any(x => x.IsStatic); //TODO: Use SystemExtensions.
				valueType = property.GetMethod.ReturnType;

				isGettable = property.CanRead && !BlockGetting;
				isSettable = property.CanWrite && !BlockSetting;
			}
			else if (null != field)
			{
				isProperty = false;
				isStatic = field.IsStatic;
				valueType = field.FieldType;

				isGettable = true && !BlockGetting;
				isSettable = true && !BlockSetting;
			}
			else
			{
				Debugger.Break();
				throw new ReflectionException($"Encountered member that was neither {typeof(PropertyInfo).Name} nor {typeof(FieldInfo).Name}");
			}
		}

		/// <summary>
		/// Get the value.
		/// </summary>
		/// <param name="instance">instance to call on (if applicable)</param>
		/// <returns>value</returns>
		public object GetValue(object instance)
		{
			var myInstance = instance;
			if (IsStatic)
				myInstance = null;

			dynamic fieldOrProperty = member;
			return fieldOrProperty.GetValue(myInstance);
		}

		/// <summary>
		/// Set the value.
		/// </summary>
		/// <param name="instance">instance to call on (if applicable)</param>
		/// <param name="data">value to set to</param>
		public void SetValue(object instance, object data)
		{
			var myInstance = instance;
			if (IsStatic)
				myInstance = null;

			dynamic fieldOrProperty = member;
			fieldOrProperty.SetValue(myInstance, data);
		}
	}
}
