
# place project specific paths in variables to make the rest of the script more generic
variables:
  PROJECT_NAME: InteractivePromptLib

  GIT_STRATEGY: fetch

  CONFIGURATION: Release
  # Configurations:
  # Release: Optimized, stripped pdb
  # Deploy:  Debug,     stripped pdb
  # Debug:   Debug,     full pdb
  #
  # Stripped pdb has the file names changed to start at the repository root.

  AUTHOR_NAME: CodeLeopard
  PACKAGE_NAME: ${PROJECT_NAME}

  PACKAGE_VERSION: "${CI_COMMIT_TAG}"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/nuget"

  # Name of directory where restore and build objects are stored.
  OBJECTS_DIRECTORY: 'obj'
  # Name of directory used for keeping restored dependencies.
  NUGET_PACKAGES_DIRECTORY: '.nuget'
  # A relative path to the source code from project repository root.
  SOURCE_CODE_PATH: '' # In this case it sits at the root.
  
  # Where inside the bin folder wil the files be located?
  # Typically bin/${CONFIGURATION}/netstandard2.0
  DLL_FOLDER: bin/${CONFIGURATION}/netstandard2.0
  BIN_FOLDER: bin/${CONFIGURATION}/netcoreapp3.1
  

  LINUX_DOTNET_SDK_IMAGE: mcr.microsoft.com/dotnet/sdk:3.1


workflow:
  rules:
    # Prevent duplicate pipelines when merge requests are involved.
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"
      when: never

    # Don't run for commits to master because master should just follow the latest release tag.
    # And the tag already triggers a pipeline.
    # The CI_COMMIT_BRANCH variable does not exist in tag pipelines so it cannot match a tag that happens to sit on master.
    - if: $CI_COMMIT_BRANCH == "master"
      when: never
    
    # Run for any other condition.
    - when: always


stages:
  - build
  - test
  - release


.build_cache: &build_cache
  before_script:
    - dotnet restore --packages $NUGET_PACKAGES_DIRECTORY
    - dotnet tool restore
  cache:
    - key: "$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG"
      policy: pull-push
      paths: 
        # Main JSON file holding information about package dependency tree, packages versions,
        # frameworks etc. It also holds information where to the dependencies ware restored.
        - '$SOURCE_CODE_PATH$OBJECTS_DIRECTORY/project.assets.json'
        # Other NuGet and MSBuild related files. Also needed.
        - '$SOURCE_CODE_PATH$OBJECTS_DIRECTORY/*.csproj.nuget.*'
        # Path to the directory where restored dependencies are kept.
        - '$NUGET_PACKAGES_DIRECTORY'


Build:
  <<: *build_cache
  stage: build
  tags:
    - linux
  image: $LINUX_DOTNET_SDK_IMAGE
  script:
    - dotnet build --configuration $CONFIGURATION --no-restore --nologo /property:WarningLevel=0
  artifacts:
    expire_in: 1 week
    paths:
      - '$PROJECT_NAME/bin/'
      - 'UTests/bin/'


TestUnit:
  <<: *build_cache
  stage: test
  tags:
    - linux
  image: $LINUX_DOTNET_SDK_IMAGE
  needs: ["Build"]
  variables:
    # We need source code for nuget to restore properly.
    # GIT_STRATEGY: none
    GIT_STRATEGY: fetch

    # Signal to tests that we are running on CI.
    TEST_CI_PRESET: 1
  script:
    # Disable exit on failure: we need the test report even if a test failed.                                                     # Store the exitCode in the same line because otherwise it doesn't work.
    - set +e
    - dotnet test --no-build --nologo --test-adapter-path:. --results-directory ./artifacts --logger:"trx;LogFilename=UTest-results.trx" ./UTests/$BIN_FOLDER/UTests.dll ; testExitCode=$?
    # ReEnable exit on error.
    - set -e

    # Note: The thingy to output JUnit report directly seems to not generate any output, so convert the trx report instead.
    - dotnet trx2junit artifacts/UTest-results.trx

    # Require artifacts ware actually generated, because in some cases it does not, causing silent failure.
    - test -f ./artifacts/UTest-results.xml

    # Fail the pipeline if any test failed.
    - echo "dotnet test exitCode = $testExitCode"
    - exit "$testExitCode"
  artifacts:
    when: always  # save test results even when the task fails
    expire_in: 1 week
    paths:
      - 'artifacts/UTest-results.xml'

    # Reports documentation.
    # https://docs.gitlab.com/ee/ci/unit_test_reports.html#net-example
    reports:
      junit: 'artifacts/UTest-results.xml'


Upload:
  # Deploy nuget package
  <<: *build_cache
  stage: release
  tags:
    - linux
  image: $LINUX_DOTNET_SDK_IMAGE
  rules:
    - if: '$CI_COMMIT_TAG =~ /\A(\.?[\w\+-]+\.?)+\z/'
  needs:
    - Build
    - job: TestUnit # Tests must succeed, but report is not required in this job.
      artifacts: false
  script:
    - dotnet pack "$PROJECT_NAME" -c $CONFIGURATION --no-build --nologo --include-symbols
    - dotnet nuget add source "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/nuget/index.json" --name gitlab --username gitlab-ci-token --password $CI_JOB_TOKEN --store-password-in-clear-text
    - dotnet nuget push "$PROJECT_NAME/bin/$CONFIGURATION/*.nupkg" --source gitlab


Release:
  # Caution, as of 2021-02-02 these assets links require a login, see:
  # https://gitlab.com/gitlab-org/gitlab/-/issues/299384
  stage: release
  tags:
    - linux
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: '$CI_COMMIT_TAG =~ /\A(\.?[\w\+-]+\.?)+\z/'
  variables:
    # We don't need source.
    GIT_STRATEGY: none
  needs: ["Upload"]
  script:
    - AUTHOR_NAME_LK=$(echo $AUTHOR_NAME | tr '[:lower:]')
    - PACKAGE_NAME_LK=$(echo $PACKAGE_NAME | tr '[:lower:]')
    - FILE_NAME="${AUTHOR_NAME_LK}.${PACKAGE_NAME_LK}.${PACKAGE_VERSION}.nupkg"
    - FULL_URL="${PACKAGE_REGISTRY_URL}/download/${AUTHOR_NAME}.${PACKAGE_NAME}/${PACKAGE_VERSION}/${FILE_NAME}"
    # Put the URL for verification
    - echo "$FULL_URL"
    - |
      release-cli create --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG --assets-link "{\"name\":\"${PROJECT_NAME}.nupkg\", \"url\":\"${FULL_URL}\"}"

