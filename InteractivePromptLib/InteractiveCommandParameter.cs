﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Reflection;

namespace InteractivePromptLib
{
	/// <summary>
	/// Provides information about the parameter of a command.
	/// </summary>
	public class InteractiveCommandParameter
	{
		internal readonly ParameterInfo parameterInfo;
		internal readonly InteractiveCommandParameterAttribute parameterAttribute;


		/// <summary>
		/// <see cref="InteractiveCommandParameterAttribute"/> attached to this Parameter, may be <see langword="null"/>
		/// </summary>
		public InteractiveCommandAttribute Parent => parameterAttribute?.Parent ?? null;

		/// <summary>
		/// <see cref="MethodInfo"/> of the method this parameter is defined on.
		/// </summary>
		public MethodInfo Method => (MethodInfo)parameterInfo.Member;

		/// <summary>
		/// <see cref="ParameterInfo"/> of the parameter this is defined on.
		/// </summary>
		public ParameterInfo ParameterInfo => parameterInfo;

		/// <summary>
		/// Name of the parameter, may be customized using the <see cref="InteractiveCommandParameterAttribute"/>.
		/// </summary>
		public string ParameterName => parameterAttribute?.NameOverride ?? parameterInfo.Name;

		/// <summary>
		/// True name of the parameter, as defined in source code.
		/// </summary>
		public string ParameterTrueName => parameterInfo.Name;

		/// <summary>
		/// The single line helpText as defined by the <see cref="InteractiveCommandParameterAttribute"/>, if that is not present it wil be <see cref="string.Empty"/>
		/// </summary>
		public string ParameterOneLineHelp => parameterAttribute?.OneLineHelp ?? string.Empty;

		/// <summary>
		/// The long helpText as defined by the <see cref="InteractiveCommandParameterAttribute"/>, if that is not present it wil be <see cref="string.Empty"/>
		/// </summary>
		public string ParameterLongHelp => parameterAttribute?.LongHelp ?? string.Empty;

		/// <inheritdoc cref="ParameterInfo.Position"/>
		public int ParameterPosition => parameterInfo.Position;

		/// <inheritdoc cref="ParameterInfo.ParameterType"/>
		public Type ParameterType => parameterInfo.ParameterType;
		/// <summary>
		/// The name of the type of this parameter
		/// </summary>
		public string ParameterTypeName => parameterInfo.ParameterType.Name;

		/// <summary>
		/// The fully qualified type name of this parameter including namespace(s), but not <see cref="Assembly"/>
		/// </summary>
		public string ParameterFullTypeName => parameterInfo.ParameterType.FullName;

		/// <summary>
		/// Does this parameter have a default value assigned to it?
		/// </summary>
		public bool HasDefault => parameterInfo.HasDefaultValue;

		/// <summary>
		/// The default value, if it's assigned. Check <see cref="HasDefault"/> tro see if it does.
		/// </summary>
		public object DefaultValue => parameterInfo.DefaultValue;


		/// <summary>
		/// 
		/// </summary>
		/// <param name="parameterInfo"></param>
		/// <param name="parameterAttribute">may be <see langword="null"/></param>
		public InteractiveCommandParameter(ParameterInfo parameterInfo, InteractiveCommandParameterAttribute parameterAttribute)
		{
			this.parameterInfo = parameterInfo ?? throw new ArgumentNullException(nameof(parameterInfo));
			this.parameterAttribute = parameterAttribute;
		}
	}
}
