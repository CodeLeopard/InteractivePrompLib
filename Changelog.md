# Changelog

## [1.3.2] - 2022-11-22

### Fixed
- Quoted string command with multiple quoted string parameters, with at least one optional one that was not specified could run index out of range and fail to process parameters.

## [1.3.0-rc.0] - 2022-09-14
Added support for Number parameters that need special NumberStyles to be parsed.

### Added
- Add `InteractiveCommandNumberParameterAttribute` that can be used to decorate parameters with `System.Globalization.NumberStyles` that will be used during parsing.
	This allows giving parameters as hexadecimal values for example.
- Add `ReflectionExt.TryParse` overload with NumberStyles parameter.

### Changed
- Modified the exception handling so that the parameter parsing Exception is displayed (so if there is a problem with the number format it'll tell you).


## [1.2.4] - 2022-06-10
Initial public release.

### Added
- License: `LGPL-3.0-or-later`
- Refactored IO so that it can be overridden to not use `System.Console`.
- Add command `ResetFormatting` to reset formatting and colors in case it got stuck.
- Add command `DebugCommand` to cause breakpoint hit just before the next command's method is invoked.
- Add command `Clear` to clear the screen.
- Add parameters to specify the Assemblies and/or Types to search for Commands.


### Changed
- Coloring now uses Virtual Terminal escape codes instead of Color API (which is only supported on Windows).


### Fixed
- Various small bugs.
- Parameter missing a Parse() method would show wrong error message.
- Encapsulated string could cause incorrectly chopped up string.



### Miscellaneous
- Removed dead dependency `CodeContracts`
- Removed non-public dependency `SystemExtensions`
- Add dependency [Pastel](https://github.com/silkfire/Pastel)
- Add GitLab CI release process
- Renamed example project to `PromptExample`


