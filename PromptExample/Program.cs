﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

using InteractivePromptLib;

namespace PromptExample
{
	internal class Program
	{
		private readonly InteractivePrompt myPrompt;
		private readonly PropertyInteraction interactor;

		[InteractableProperty()]
		private int field1 = 1;

		[InteractableProperty()]
		private float Property1 { get; set; }

		[InteractableProperty()]
		private int PropertyRead { get; }


		Program()
		{
			myPrompt = new InteractivePrompt(
				typeof(MyCommandAttribute),
				this,
				"Program>",
				searchAssemblies: Enumerable.Empty<Assembly>(),
				searchClasses: Yield(typeof(Program)));
			interactor = new PropertyInteraction(typeof(InteractablePropertyAttribute), this, true);
			PropertyRead = 66;
		}

		public static IEnumerable<T> Yield<T>(T item)
		{
			yield return item;
		}

		[MyCommand(oneLineHelp: "Get the value of a field or property with the given name.")]
		public void Get(string name = null, bool includeHidden = false)
		{
			interactor.Get(name, includeHidden);
		}

		[MyCommand(oneLineHelp: "Set the value of a field or property with the given name.")]
		public void Set(string[] data = null)
		{
			interactor.Set(data);
		}


		[MyCommand(oneLineHelp: "Execute a function with void signature.")]
		public void VoidFunction()
		{
			Console.WriteLine("void");
		}

		[MyCommand(oneLineHelp: "Execute a function with void (int) signature.")]
		public void IntFunction(int i)
		{
			field1 += i;
			Console.WriteLine(field1);
		}

		[MyCommand(oneLineHelp: "Execute a function with void (string[]) signature.")]
		public void StringArrayFunction(string[] arguments)
		{
			foreach(var argument in arguments)
			{
				Console.WriteLine(argument);
			}
		}

		[MyCommand(oneLineHelp: "Execute a function with void (string, string) signature supporting quoted strings with spaces.")]
		private void TwoStringFunction([InteractiveCommandStringParameter(encapsulationCharacters:"\"'")] string a,
		                               [InteractiveCommandStringParameter(encapsulationCharacters: "\"'")] string b = null)
		{
			Console.WriteLine($"source: {a}\ndestination: {b ?? "null"}");
		}

		[MyCommand(oneLineHelp: "Function that uses the auto print return value functionality.", showReturnValue: true)]
		public int ReturningFunction(int i)
		{
			return i;
		}

		[MyCommand(oneLineHelp: "Function that returns a value but auto print return value functionality is DISABLED.", showReturnValue: false)]
		public int ReturningFunctionDisabled(int i)
		{
			return i;
		}

		[MyCommand(oneLineHelp: "Tests the parameter annotations.")]
		public void ParameterHelpTest
			([InteractiveCommandParameter(nameOverride: "boo", oneLineHelp: "foo does foo stuff")]int foo
			, [InteractiveCommandParameter(oneLineHelp: "bar does bar stuff")]string bar
			, [InteractiveCommandParameter(oneLineHelp: "baz does baz stuff")] float baz)
		{
			Console.WriteLine(string.Format(System.Globalization.CultureInfo.InvariantCulture, "foo: {0}, bar: {1}, baz: \"{2}\"", foo, bar, baz));
		}

		[MyCommand(oneLineHelp: "Takes input as hexadecimal")]
		public void NumberStylesTest([InteractiveCommandNumberParameter(numberStyles: NumberStyles.AllowHexSpecifier)] UInt32 hexInput)
		{
			Console.WriteLine(hexInput);
		}


		private void Run()
		{
			try
			{
				myPrompt.Run();
			}
			catch (Exception e)
			{
				Console.Error.WriteLine(e.ToString());
				Console.WriteLine("Program Crashed! Press Enter to Exit");
				Console.ReadLine();
				throw;
			}
		}

		private static void Main(/*string[] args*/)
		{
			new Program().Run();
		}
	}
}
