﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Tests.Examples
{
	public class TestPrompt : InteractivePromptLib.InteractivePrompt
	{
		/// <inheritdoc />
		public TestPrompt
		(
			Type                  attribute
		  , object                instance
		  , string                prompt           = "Prompt>"
		  , bool                  ignoreCase       = true
		  , IEnumerable<Assembly> searchAssemblies = null
		  , IEnumerable<Type>     searchClasses    = null
		) : base(attribute, instance, prompt, ignoreCase, searchAssemblies, searchClasses)
		{

		}

		// todo: override IO to streams.
	}
}
