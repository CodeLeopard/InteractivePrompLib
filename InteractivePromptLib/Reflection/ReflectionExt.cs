﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace InteractivePromptLib.Reflection
{
	internal static class ReflectionExt
	{
		// Relevant parts taken from SystemExtensions.

		/// <summary>
		/// Finds all derived <see cref="Type"/>s of <typeparamref name="TBase"/> in the current <see cref="AppDomain"/>.
		/// </summary>
		/// <typeparam name="TBase">The type to search derivatives of</typeparam>
		/// <returns>List of Types</returns>
		public static IEnumerable<Type> FindAllDerivedTypes<TBase>() where TBase : class
		{
			foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				foreach (var type in FindAllDerivedTypes<TBase>(assembly))
				{
					yield return type;
				}
			}
		}

		/// <summary>
		/// Finds all derived <see cref="Type"/>s of <typeparamref name="TBase"/> in the given <see cref="Assembly"/>.
		/// </summary>
		/// <typeparam name="TBase">The type to search derivatives of</typeparam>
		/// <param name="assembly">Assembly to search in</param>
		/// <returns>List of Types</returns>
		public static IEnumerable<Type> FindAllDerivedTypes<TBase>(Assembly assembly) where TBase : class
		{
			_ = assembly ?? throw new ArgumentNullException(nameof(assembly));

			var baseType = typeof(TBase);
			return assembly
				.GetTypes()
				.Where(t =>
					t != baseType &&
					t.IsSubclassOf(baseType)
					);
			// exclude the type other types need to be derived from
		}

		/// <summary>
		/// Finds all <see cref="Type"/>s that implement <typeparamref name="TInterface"/> in the given <see cref="Assembly"/>.
		/// </summary>
		/// <typeparam name="TInterface">The interface to search implementations of</typeparam>
		/// <param name="assembly">Assembly to search in</param>
		/// <returns>List of Types</returns>
		public static IEnumerable<Type> FindAllImplementationsOf<TInterface>(Assembly assembly) where TInterface : class
		{
			_ = assembly ?? throw new ArgumentNullException(nameof(assembly));

			var interfaceType = typeof(TInterface);
			if (false == interfaceType.IsInterface)
				throw new ArgumentException("Type parameter must be an Interface");

			return assembly.GetTypes()
				.Where(implementingType =>
					implementingType != interfaceType
				 && interfaceType.IsAssignableFrom(implementingType));
		}

		/// <summary>
		/// Finds members in <paramref name="inType"/> that are decorated with a <paramref name="attribute"/>.
		/// </summary>
		/// <param name="inType">Type to search in</param>
		/// <param name="attribute">Attribute to find</param>
		/// <param name="bindingFlags">bindingFlags</param>
		/// <param name="inherit">search ancestors of <paramref name="inType"/></param>
		/// <returns>List of <see cref="MemberInfo"/> (and <see cref="Attribute"/>, in case a derived attribute was found)</returns>
		public static IEnumerable<(MemberInfo, Attribute)> FindMembersWithAttribute(
			Type inType,
			Type attribute,
			BindingFlags bindingFlags = BindingFlags.DeclaredOnly | BindingFlags.Public,
			bool inherit = false)
		{
			_ = inType ?? throw new ArgumentNullException(nameof(inType));
			_ = attribute ?? throw new ArgumentNullException(nameof(attribute));
			if (false == attribute.IsSubclassOf(typeof(Attribute)))
				throw new ArgumentException($"Parameter {nameof(attribute)} should be a subclass of 'System.Attribute'");

			foreach (var member in inType.GetMembers(bindingFlags))
			{
				var myAttribute = member.GetCustomAttribute(attribute, inherit);
				if (myAttribute != null)
				{
					yield return (member, myAttribute);
				}
			}
		}

		/// <summary>
		/// Finds members in <paramref name="inType"/> that have <paramref name="attribute"/> in their <see langword="struct"/> or <see langword="class"/> definition.
		/// </summary>
		/// <param name="inType">The type to search in</param>
		/// <param name="attribute">Attribute to find</param>
		/// <param name="bindingFlags">bindingFlags</param>
		/// <param name="inherit">search ancestors of <paramref name="inType"/></param>
		/// <returns>List of <see cref="MemberInfo"/> (and <see cref="Attribute"/>, in case a derived attribute was found)</returns>
		public static IEnumerable<(MemberInfo, Attribute)> FindMembersWithAttributeInTheirType(
			Type inType,
			Type attribute,
			BindingFlags bindingFlags = BindingFlags.DeclaredOnly | BindingFlags.Public,
			bool inherit = false)
		{
			_ = inType ?? throw new ArgumentNullException(nameof(inType));
			_ = attribute ?? throw new ArgumentNullException(nameof(attribute));
			if (false == typeof(Attribute).IsAssignableFrom(attribute))
				throw new ArgumentException($"Parameter {nameof(attribute)} should be assignable from the Type '{nameof(Attribute)}'");

			foreach (var member in inType.GetMembers(bindingFlags))
			{
				Type memberType = member.ReflectedType;
				var myAttribute = memberType.GetCustomAttribute(attribute, inherit);

				if (myAttribute != null)
				{
					yield return (member, myAttribute);
				}
			}
		}

		/// <summary>
		/// Finds the first occurrence of a field with type toFind in Type toSearch, <see langword="null"/> if none found.
		/// </summary>
		/// <param name="toSearch">The Type to search in.</param>
		/// <param name="toFind">The Type of the field to find.</param>
		/// <returns>first found or <see langword="null"/> if none found.</returns>
		public static FieldInfo FindFieldInType(Type toSearch, Type toFind,
			BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
		{
			_ = toSearch ?? throw new ArgumentNullException(nameof(toSearch));
			_ = toFind ?? throw new ArgumentNullException(nameof(toFind));

			foreach (var mi in toSearch.GetMembers(bindingFlags))
			{
				if (mi.MemberType == MemberTypes.Field)
				{
					var field = (FieldInfo)mi;
					if (field.FieldType == toFind)
						return field;
				}
			}
			return null;
		}

		/// <summary>
		/// Finds the first occurrence of a property with type toFind in Type toSearch, <see langword="null"/> if none found.
		/// </summary>
		/// <param name="toSearch">The Type to search in.</param>
		/// <param name="toFind">The Type of the property to find.</param>
		/// <returns>First found or <see langword="null"/> if none found.</returns>
		public static PropertyInfo FindPropertyInType(Type toSearch, Type toFind,
			BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
		{
			_ = toSearch ?? throw new ArgumentNullException(nameof(toSearch));
			_ = toFind ?? throw new ArgumentNullException(nameof(toFind));

			foreach (var mi in toSearch.GetMembers(bindingFlags))
			{
				if (mi.MemberType == MemberTypes.Property)
				{
					var field = (PropertyInfo)mi;
					if (field.PropertyType == toFind)
						return field;
				}
			}
			return null;
		}

		/// <summary>
		/// Will Try to Parse() whatever type you give it.
		/// </summary>
		/// <remarks>
		/// Supports "MinValue" and "MaxValue" as inputs too.
		/// </remarks>
		/// <param name="t">Type</param>
		/// <param name="value">The String to Parse()</param>
		/// <param name="result">The result of the parse operation, if Failed it may contain the exception that occurred.</param>
		/// <returns><see langword="true"/> if Success, <see langword="false"/> if Failed.</returns>
		public static bool TryParse(Type t, string value, out dynamic result)
		{
			_ = t ?? throw new ArgumentNullException(nameof(t)); //Contract.Requires(null != t);
			_ = value ?? throw new ArgumentNullException(nameof(value)); //Contract.Requires(null != value);

			// Try to find a field with Min- or Max-Value.
			if (value == "MinValue" || value == "MaxValue")
			{
				if (TryGetFieldValue(t, value, null, out result))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			// Try to find Parse() method.
			var searchParameters = new Type[2];
			searchParameters[0] = typeof(string);
			searchParameters[1] = typeof(NumberStyles); // Assume it's a number
			var ParseMethod = t.GetMethod("Parse", searchParameters);
			if (null == ParseMethod)
			{
				// Try not a number
				searchParameters = new Type[1];
				searchParameters[0] = typeof(string);
				ParseMethod = t.GetMethod("Parse", searchParameters);

				if (null == ParseMethod)
				{
					result = new ArgumentException($"Type '{t.Name}' does not have a (usable) Parse() Method.");
					return false;
				}
			}

			// Invoke parse method
			object[] methodParameters = new object[searchParameters.Length];
			methodParameters[0] = value;
			if (methodParameters.Length == 2)
				methodParameters[1] = NumberStyles.Number | NumberStyles.Float;
			try
			{
				result = ParseMethod.Invoke(null, methodParameters);
			}
			catch (Exception e)
			{
				result = e;
				return false;
			}
			return true;
		}

		/// <summary>
		/// Will Try to Parse() whatever type you give it.
		/// </summary>
		/// <remarks>
		/// Supports "MinValue" and "MaxValue" as inputs too.
		/// </remarks>
		/// <param name="t">Type</param>
		/// <param name="value">The String to Parse()</param>
		/// <param name="result">The result of the parse operation, if Failed it may contain the exception that occurred.</param>
		/// <returns><see langword="true"/> if Success, <see langword="false"/> if Failed.</returns>
		public static bool TryParse(Type t, string value, NumberStyles styles, out dynamic result)
		{
			_ = t ?? throw new ArgumentNullException(nameof(t)); //Contract.Requires(null != t);
			_ = value ?? throw new ArgumentNullException(nameof(value)); //Contract.Requires(null != value);

			// Try to find a field with Min- or Max-Value.
			if (value == "MinValue" || value == "MaxValue")
			{
				if (TryGetFieldValue(t, value, null, out result))
				{
					return true;
				}
				else
				{
					return false;
				}
			}

			if ((styles & NumberStyles.AllowHexSpecifier) != 0)
			{
				value = value.Replace("0x", "").Replace("0X", "");
			}

			// Try to find Parse() method.
			var searchParameters = new Type[2];
			searchParameters[0] = typeof(string);
			searchParameters[1] = typeof(NumberStyles);
			var ParseMethod = t.GetMethod("Parse", searchParameters);
			if (null == ParseMethod)
			{
				result = new ArgumentException($"Type '{t.Name}' does not have a (usable) Parse() Method with NumberStyles parameter.");
				return false;
			}

			// Invoke parse method
			object[] methodParameters = new object[searchParameters.Length];
			methodParameters[0] = value;
			methodParameters[1] = styles;
			try
			{
				result = ParseMethod.Invoke(null, methodParameters);
			}
			catch (Exception e)
			{
				result = e;
				return false;
			}
			return true;
		}

		/// <summary>
		/// Will try to get the value of a field in the provided <see cref="Type"/>.
		/// </summary>
		/// <param name="t"><see cref="Type"/> to inspect.</param>
		/// <param name="fieldName">The na me of the field to find.</param>
		/// <param name="instance">The instance to operate on, or <see langword="null"/> for static field.</param>
		/// <param name="result">The value of the field, or if Failed the exception that occurred.</param>
		/// <returns><see langword="true"/> if Success, <see langword="false"/> if Failed.</returns>
		public static bool TryGetFieldValue(Type t, string fieldName, object instance, out dynamic result)
		{
			_ = t ?? throw new ArgumentNullException(nameof(t));
			if (string.IsNullOrWhiteSpace(fieldName)) throw new ArgumentNullException(nameof(fieldName), "Was null or whitespace");

			var flags = BindingFlags.Public | BindingFlags.NonPublic;

			if (null == instance)
				flags |= BindingFlags.Static;
			else
				flags |= BindingFlags.Instance;

			var fieldInfo = t.GetField(fieldName, flags);
			if (fieldInfo == null)
			{
				result = new InvalidOperationException($"The type {t.FullName} does not have a {fieldName} field!");
				return false;
			}

			try
			{
				result = fieldInfo.GetValue(instance);
			}
			catch (Exception e)
			{
				result = e;
				return false;
			}

			return true;
		}
	}
}
