InteractivePromptLib
=
A C# library for creating an interactive prompt by annotating methods with attributes.

About
-
This library allows you to easily create an interactive command line application around an existing library by annotating the library methods (and their parameters) with attributes. Types that have a Parse(string) method will be automatically parsed. Quoted strings can be extracted to supply (multiple) strings with spaces to a method.

Disclaimer
-
This project was made to suit my specific needs and is not really intended for use beyond that. I do **not** promise to provide support for this library and I may introduce significant breaking changes in future versions.

Get
-
Head over to the [Package Registry](https://gitlab.com/CodeLeopard/InteractivePrompLib/-/packages) to see releases.

Usage
-
In the library project:
```CSharp
[InteractiveCommand(
	oneLineHelp: "Computes the length of the provided string.",
	showReturnValue: true)]
static int StringLength(
	[InteractiveCommandParameter(
		nameOverride: "input",
		oneLineHelp: "The string to compute the length of.")]
	string arg1)
{
	return arg1.length;
}
```
In the Console app:
```CSharp
static void Main()
{
	var prompt = new InteractivePrompt(typeof(InteractiveCommandAttribute), null, "Program>");
	prompt.Run();
}

```
On Startup the InteractivePrompt will search through all Assemblies in the current domain for members with the provided attribute. For static methods that's all there is to it.

To support instance methods:
```CSharp
static void Main()
{
	var instance = new LibraryClass();
	var prompt = new InteractivePrompt(typeof(InteractiveCommandAttribute), instance, "Program>");
	prompt.Run();
}

```
When an instance is required first `instance` will checked for the correct type, if not matched all fields and properties will be searched for a member of the required type (**not** recursive) the first match is provided as the object for the Method to operate on. This only works for singleton instances, to support multiple instances of the same Type you could create multiple `InteractivePrompt`s each handling an instance.

Methods that return a value will (by default) have that value ToString()ed and shown in the console.

Parameters with default values can be omitted and the default value will be provided.

Quoted string example:
```CSharp
[MyCommand(oneLineHelp: "Execute a function with void (string, string) signature supporting quoted strings with spaces.")]
static void TwoStringFunction
(
	[InteractiveCommandStringParameter(encapsulationCharacters:"\"'")]
	string a,
	[InteractiveCommandStringParameter(encapsulationCharacters: "\"'")]
	string b = null
)
{
	Console.WriteLine($"source: {a}\ndestination: {b ?? "null"}");
}
```
The string is provided to the method without the quotes.
Note that there is a bug where, when multiple encapsulation characters are allowed they can be mixed between opening and closing, see #2.


Built-in commands
-
- Help: Displays information about a specific command, or a generic help message if no command is provided.
- ListCommands: Displays the available commands.
- Aliases: Lists the aliases for commands.
- Quit: Exit the current interactive prompt.
- Clear: Clear the screen.
- ColorEnabled: Enable or disable virtual terminal color codes, or see the current value.
- ResetFormatting: Writes a virtual terminal code to reset formatting like colors.
- DebugCommand: Launches a debugger and, when another command is entered, causes a breakpoint to hit just before the target method is invoked.



License
-
`LGPL-3.0-or-later` See COPYING and COPYING.LESSER