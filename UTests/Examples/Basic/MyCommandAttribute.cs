﻿using System;
using System.Collections.Generic;
using System.Text;

using InteractivePromptLib;

namespace Tests.Examples.Basic
{
	internal class MyCommandAttribute : InteractiveCommandAttribute
	{
		/// <inheritdoc />
		public MyCommandAttribute(string commandNameOverride = null, string oneLineHelp = null, string longHelp = null, string[] aliases = null, bool showReturnValue = true, bool disabled = false, bool hidden = false)
			: base(commandNameOverride, oneLineHelp, longHelp, aliases, showReturnValue, disabled, hidden)
		{ }
	}
}
