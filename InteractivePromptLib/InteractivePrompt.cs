// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

using InteractivePromptLib.Reflection;

using Pastel;

namespace InteractivePromptLib
{
	/// <summary>
	/// This class will handle the automatic creation of an interactive prompt using reflection and the <see cref="InteractiveCommandAttribute"/>
	/// </summary>
	public class InteractivePrompt
	{
		/// <summary>
		/// The prompt that appears before the cursor when a command can be entered.
		/// </summary>
		public string prompt;

		/// <summary>
		/// Ignore case when looking for a command.
		/// </summary>
		public readonly bool ignoreCase;



		protected Dictionary<string, (MethodInfo, InteractiveCommandAttribute)> mInvokeDict =
			new Dictionary<string, (MethodInfo, InteractiveCommandAttribute)>();

		protected Dictionary<string, string> AliasToName = new Dictionary<string, string>();

		/// <summary>
		/// The attribute this prompt will create it's commands from.
		/// </summary>
		protected Type attributeType;

		protected Type containerAttributeType;

		/// <summary>
		/// The instance that will be used
		/// </summary>
		protected object instance;

		protected bool run;


		private bool _colors = true;

		/// <summary>
		/// Is coloring of output enabled.
		/// </summary>
		public bool Colors
		{
			get => _colors;
			set
			{
				if(value == _colors) return;

				_colors = value;

				if (_colors)
				{
					ConsoleExtensions.Enable();
				}
				else
				{
					ConsoleExtensions.Disable();
				}
			}
		}

		/// <summary>
		/// Call <see cref="Debugger.Break"/> just before the next Command's target method is Invoked.
		/// </summary>
		public bool DebugNextCommand;

		/// <summary>
		/// Create a new Interactive prompt.
		/// </summary>
		/// <param name="attribute">The attribute type to work with</param>
		/// <param name="instance">The instance to use when invoking commands, null if static class</param>
		/// <param name="prompt"></param>
		/// <param name="ignoreCase"></param>
		/// <param name="searchAssemblies">
		/// <see cref="Assembly"/> to search through or <see langword="null"/> to search through <see cref="AppDomain.CurrentDomain"/>.
		/// To use only <paramref name="searchClasses"/> provide <see cref="Enumerable.Empty{Assembly}"/>.
		/// </param>
		/// <param name="searchClasses"><see cref="Type"/>s to inspect for commands, or <see langword="null"/> to search all types in <paramref name="searchAssemblies"/>.</param>
		public InteractivePrompt(
			Type attribute,
			object instance,
			string prompt = "Prompt>",
			bool ignoreCase = true,
			IEnumerable<Assembly> searchAssemblies = null,
			IEnumerable<Type> searchClasses = null
		)
		{
			this.attributeType = attribute;
			this.instance = instance;
			this.prompt = prompt;
			this.ignoreCase = ignoreCase;

			Initialize(searchAssemblies: searchAssemblies, searchClasses: searchClasses);
		}

		private void Initialize(IEnumerable<Assembly> searchAssemblies = null, IEnumerable<Type> searchClasses = null)
		{
			_ = attributeType ?? throw new InvalidOperationException($"{nameof(attributeType)} should not be null.");

			if(! typeof(InteractiveCommandAttribute).IsAssignableFrom(attributeType))
				throw new InvalidOperationException($"Parameter \"attribute\" should be assignable from the Type \"{nameof(InteractiveCommandAttribute)}\"");

			var commands = new List<(MethodInfo, Attribute)>();

			// Find commands defined in this class (the internal commands) and add them.
			var privateFound = ReflectionExt.FindMembersWithAttribute(this.GetType(),
				typeof(InternalInteractiveCommandAttribute),
				BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);

			foreach (var tuple in privateFound)
			{
				var correctType = ((MethodInfo) tuple.Item1, tuple.Item2);
				commands.Add(correctType);
			}

			// Search for the provided attribute.
			searchAssemblies = searchAssemblies ?? AppDomain.CurrentDomain.GetAssemblies();

			searchClasses = searchClasses == null
				? searchAssemblies.SelectMany(a => a.GetTypes())
				: searchClasses.Concat(searchAssemblies.SelectMany(a => a.GetTypes()));

			foreach (Type t in searchClasses)
			{
				var found = ReflectionExt.FindMembersWithAttribute(t, attributeType,
					BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static |
					BindingFlags.NonPublic | BindingFlags.Public);
				foreach (var tuple in found)
				{
					if (tuple.Item2 is InteractiveCommandAttribute attribute
					 && attribute.GetType() == attributeType
					 && false               == attribute.Disabled)
					{
						commands.Add(((MethodInfo)tuple.Item1, tuple.Item2));
					}
					else
					{
						// Attribute must match exactly.
						// Attribute must not be disabled.
						continue;
					}
				}
			}

			// Add the commands found by the attribute to the dictionary.
			foreach (var tup in commands)
			{
				var mInfo = tup.Item1;
				var typedAtt = (InteractiveCommandAttribute) tup.Item2;

				typedAtt.SetMethodInfo(mInfo);

				var commandName = typedAtt.CommandName;
				if (ignoreCase)
					commandName = commandName.ToLowerInvariant();

				mInvokeDict.Add(commandName, (tup.Item1, typedAtt));
				Debug.WriteLine($"Registered Interactive-Command-Method {tup.Item1.DeclaringType.Name}.{tup.Item1.Name} under name: {commandName}");
			}

			// Add aliases, this must be done afterwards so we can handle name collisions.
			foreach (var tup in commands)
			{
				var typedAtt = (InteractiveCommandAttribute) tup.Item2;
				if (null != typedAtt.Aliases)
				{
					foreach (var aliasCased in typedAtt.Aliases)
					{
						var alias = aliasCased;
						if (ignoreCase)
							alias = aliasCased.ToLowerInvariant();

						if (mInvokeDict.ContainsKey(alias))
						{
							Debug.WriteLine($"Unable to add alias:\"{alias}\" because that name already exists.");
						}
						else
						{
							AliasToName.Add(alias, typedAtt.CommandName);
							mInvokeDict.Add(alias, (tup.Item1, typedAtt));
						}
					}
				}
			}
		}


		/// <summary>
		/// Start the interactive session, blocks the thread until exit.
		/// Re-Entry is supported.
		/// </summary>
		/// <param name="announcement"></param>
		public void Run(string announcement = "Starting interactive session...")
		{
			run = true;
			bool suppressPrompt = false;
			WriteLine(announcement);
			while (run)
			{
				if (false == suppressPrompt)
					Write(prompt);
				suppressPrompt = false;
				var input = ReadLine();

				var words = input.Split();
				if (words.Length == 0) continue; // Empty Line
				var command = words[0];
				if (ignoreCase)
					command = command.ToLowerInvariant();
				try
				{
					bool found = mInvokeDict.TryGetValue(command, out var tuple);
					if (found)
					{
						MyInvoke(tuple.Item1, tuple.Item2, words, input);
						continue;
					}

					WriteLine("Command unknown.");
				}
				catch (Exception e)
				{
					WriteLine($"{e.GetType().Name}: {e.Message}".Pastel(Color.Red));
					Debug.Print($"{e.GetType().Name} occurred while evaluating command '{command}':\n{e}");
				}
			}
		}


		private static readonly Dictionary<string, Regex> UnEncapsulationRegexMap = new Dictionary<string, Regex>();


		private static Regex GetOrCreateRegex(string encapsulationCharacters)
		{
			if (UnEncapsulationRegexMap.TryGetValue(encapsulationCharacters, out Regex result))
			{
				return result;
			}

			string escaped = Regex.Escape(encapsulationCharacters);

			/*
			^  Only match if the opening quote is the start of the string
			   (if we don't we would find *any* quoted parameter, not the one we are currently looking for).
			 [{escaped}]  match a single of any of the escape characters.
			            (  begin capture group for content.
			             [^{escaped}]  match anything that is not the encapsulation.
			                         *  zero or more times.
			                          )  end capture group for content.
			                           [{escaped}]  match a single of any of the escape characters.
			*/
			// todo: when there are multiple encapsulation characters this regex will match 'foo bar" which is technically correct but not desired.
			// todo: escaping of encapsulation should be supported.
			Regex unEncapsulationRegex = new Regex($"^[{escaped}]([^{escaped}]*)[{escaped}]", RegexOptions.Compiled);

			UnEncapsulationRegexMap[encapsulationCharacters] = unEncapsulationRegex;

			return unEncapsulationRegex;
		}

		private static bool CheckAndReset(ref bool doTrap)
		{
			if (! doTrap) return false;

			doTrap = false;
			return true;
		}

		/// <summary>
		/// The method used to invoke the actual Command's Method.
		/// </summary>
		/// <param name="info"></param>
		/// <param name="inputWords"></param>
		protected void MyInvoke(MethodInfo info, InteractiveCommandAttribute attribute, string[] inputWords, string fullInput)
		{
			_ = info       ?? throw new ArgumentNullException(nameof(info));
			_ = attribute  ?? throw new ArgumentNullException(nameof(attribute));
			_ = inputWords ?? throw new ArgumentNullException(nameof(inputWords));
			_ = fullInput  ?? throw new ArgumentNullException(nameof(fullInput));

			_ = attributeType ?? throw new InvalidOperationException($"{nameof(attributeType)} should not be null");

			string[] inputParameters = inputWords.Skip(1).ToArray();


			// TODO: performance?


			object SpecificInstance = GetInvokeTargetInstance(info);

			ParameterInfo[] paramInfos = info.GetParameters();
			InteractiveCommandParameterAttribute[] methodParameterDetails
				= attribute.Parameters
				           .Select(p => p.parameterAttribute)
				           .ToArray();

			object[] paramValues = new object[paramInfos.Length];

			// Special case: input is string array.
			// todo: support quoted strings.
			if (paramValues.Length == 1 && paramInfos[0].ParameterType == typeof(string[]))
			{
				if (inputWords.Length > 1)
					paramValues[0] = inputWords.Skip(1).ToArray(); // Skip(1): We don't pass the command itself.
				// else it stays null.


				if (CheckAndReset(ref DebugNextCommand)) Debugger.Break();

				var invokeResult = info.Invoke(SpecificInstance, paramValues);
				if (invokeResult != null && attribute.ShowReturnValue)
				{
					WriteObject(invokeResult);
				}
				return;

				// else: go to the for loop below.
			}

			int fullInputStartIndex = inputWords[0].Length + 1;
			int inputParamsSkipped = 0;

			// Try to parse each of the parameters into the expected type of the method.
			for (int i = 0; i < paramValues.Length; i++)
			{
				if (paramInfos[i].ParameterType == typeof(string) && i < inputParameters.Length)
				{
					if (methodParameterDetails[i] is InteractiveCommandStringParameterAttribute details)
					{
						// We need to do encapsulation, like a 'quoted path'

						Regex unEncapsulator = GetOrCreateRegex(details.EncapsulationCharacters);

						if(fullInputStartIndex >= fullInput.Length)
						{
							// We went out of range of the input string.
							// No furhter parameters will be specified.
							break;
						}

						string matchTarget = fullInput.Substring(fullInputStartIndex);
						var match = unEncapsulator.Match(matchTarget);

						if (match.Success)
						{
							paramValues[i] = match.Groups[1].Value;

							// Count how many 'parameters' in the array we need to skip due to the quoting.
							int count = match.Value.Count(s => s == ' ');
							inputParamsSkipped += count;

							fullInputStartIndex += match.Length + 1; // +1 for space after match
							continue;
						}
					}


					if (i + inputParamsSkipped < inputParameters.Length)
					{
						paramValues[i] = inputParameters[i + inputParamsSkipped];
					}
					// else it stays null.
				}
				else if (ParseOrDefault(i, inputParamsSkipped))
				{
					// the method did stuff
					// TODO: refactor this because it's not nice to read.
				}
				else
				{
					throw new ArgumentException($"The command has a parameter '{paramInfos[i].Name}' (#{i}) of Type {paramInfos[i].ParameterType.Name} for which there is no default value: Unable to invoke the Method.");
				}
				if(i + inputParamsSkipped < inputParameters.Length)
					fullInputStartIndex += inputParameters[i + inputParamsSkipped].Length + 1; // +1 for the missing separator space.
			}


			try
			{
				if (CheckAndReset(ref DebugNextCommand)) Debugger.Break();

				var invokeResult = info.Invoke(SpecificInstance, paramValues);

				if (invokeResult != null && attribute.ShowReturnValue)
				{
					WriteObject(invokeResult);
				}
			}
			catch (TargetInvocationException e)
			{
				if (e.InnerException != null)
					throw e.InnerException;
				throw;
			}

			return;

			// #### #### #### #### #### #### #### #### #### ####
			// #### End of method, internal methods below.  ####
			// #### #### #### #### #### #### #### #### #### ####

			bool ParseOrDefault(int i, int skipped)
			{
				if (! (i >= 0)) throw new ArgumentOutOfRangeException(nameof(i), i, $">= 0");

				if (i + skipped < inputParameters.Length) // won't go out of range for input param.
				{
					Type t = paramInfos[i].ParameterType;
					if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>))
					{
						// Support for Nullable<T>
						var typeArgs = t.GetGenericArguments();
						t = typeArgs[0];
					}

					dynamic result;
					if (methodParameterDetails[i] is InteractiveCommandNumberParameterAttribute details)
					{
						if (ReflectionExt.TryParse(t, inputParameters[i + skipped], details.NumberStyles, out result))
						{
							paramValues[i] = result;
							return true;
						}
					}
					else if (ReflectionExt.TryParse(t, inputParameters[i + skipped], out result))
					{
						paramValues[i] = result;
						return true;
					}

					if(result is Exception e // todo: this is crap.
							&& ! paramInfos[i].HasDefaultValue)
					{
						if (e is TargetInvocationException targetInvocationException)
						{
							e = targetInvocationException.InnerException;
						}

						throw new ArgumentException($"The parameter '{paramInfos[i].Name}' (#{i}) of Type {paramInfos[i].ParameterType.Name} cannot be parsed from string '{inputParameters[i + skipped]}' and has no default value: Unable to invoke the Method.\n ---> {e.GetType().Name}: {e.Message}", e);
					}
				}

				// not p or p did not return (failed)
				if (paramInfos[i].HasDefaultValue)
				{
					paramValues[i] = Type.Missing;
					return true;
				}

				return false;
			}
		}

		private object GetInvokeTargetInstance(MethodInfo info)
		{
			// Find the class or instance to call the method on.
			var wantedInstanceType = info.DeclaringType;
			object SpecificInstance;
			if (wantedInstanceType == typeof(InteractivePrompt))
			{
				// Special case: internal command.
				SpecificInstance = this;
			}
			else if (false == info.IsStatic && instance.GetType() != wantedInstanceType)
			{
				// the method is not in typeof(instance) or typeof(this): we will search for it in the fields and properties of instance (does NOT recurse).
				PropertyInfo prop = null;

				SpecificInstance = null;

				string name = "--## Unknown ##--"; // So that the exceptions below don't get null / empty.

				Exception lastException = null;

				bool found = false;

				IEnumerable<Type> TypesToSearch()
				{
					_ = instance ?? throw new InvalidOperationException($"{nameof(instance)} was null");

					yield return instance.GetType();
					if (null != containerAttributeType)
					{
						foreach (var tuple in ReflectionExt.FindMembersWithAttributeInTheirType
							         (instance.GetType(), containerAttributeType, inherit: true))
						{
							var myMemberInfo = tuple.Item1;
							var myAttribute = tuple.Item2;
							if (myMemberInfo.ReflectedType == wantedInstanceType
							 && myAttribute.GetType()      == containerAttributeType)
							{
								yield return myMemberInfo.ReflectedType;
							}
						}
					}
				}
				foreach (var type in TypesToSearch())
				{
					lastException = null;
					name = "--## Unknown ##--"; // So that the exceptions below don't get null / empty.

					FieldInfo field = ReflectionExt.FindFieldInType(instance.GetType(), wantedInstanceType);
					if (field == null) prop = ReflectionExt.FindPropertyInType(instance.GetType(), wantedInstanceType);

					try
					{
						if (field != null)
						{
							name = field.Name;
							SpecificInstance = field.GetValue(instance);
							found = true;
							break;
						}
						else if (prop != null)
						{
							name = prop.Name;
							SpecificInstance = prop.GetValue(instance);
							found = true;
							break;
						}
						else
						{
							continue;
						}
					}
					catch (Exception e)
					{
						lastException = e;
						continue;
					}
				}

				if (found && SpecificInstance == null)
				{
					throw new ReflectionException($"The field or property {name} in our instance of "
					                            + $"{instance.GetType().Name} with the Type {wantedInstanceType.Name} "
					                            + $"was found but has a value of null."
					                            , lastException
					                             );
				}

				_ = SpecificInstance
				 ?? throw new ReflectionException($"Could not find a field or property in our instance of {instance.GetType().Name} "
				                                + $"or any of the objects marked with {containerAttributeType.Name}"
				                                , lastException
				                                 );
			}
			else if (info.IsStatic)
			{
				SpecificInstance = null;
			}
			else
			{
				SpecificInstance = instance;
			}

			return SpecificInstance;
		}




		#region IO

		protected virtual void Write(string value)
		{
			Console.Write(value);
		}

		protected virtual void Write(object value)
		{
			Write(value.ToString());
		}

		protected virtual void WriteLine(string value)
		{
			Console.WriteLine(value);
		}

		protected virtual void WriteLine(object value)
		{
			Console.WriteLine(value.ToString());
		}

		protected virtual void WriteLine()
		{
			Console.WriteLine("\n");
		}

		protected virtual string ReadLine()
		{
			return Console.ReadLine();
		}

		protected virtual void SeekStartOfLine()
		{
			Console.SetCursorPosition(0, Console.CursorTop);
		}

		protected virtual void ClearLine()
		{
			SeekStartOfLine();
			Write(new string(' ', Console.BufferWidth));
			SeekStartOfLine();
		}

		protected virtual void ClearScreen()
		{
			Console.Clear();
		}


		/// <summary>
		/// Writes a message to the Console, without interfering with the user.
		/// </summary>
		/// <param name="message"></param>
		public void WriteMessage(string message)
		{
			ClearLine();
			WriteLine(message);
			Write(prompt);
		}

		/// <summary>
		/// Writes Object to the console, with optional messages before and after.
		/// </summary>
		/// <param name="messageObject">The Object to write</param>
		/// <param name="appendMessage">message to put after the object</param>
		/// <param name="prependMessage">message to put before the object</param>
		public void WriteObject(object messageObject, string prependMessage = "", string appendMessage = "")
		{
			if (prependMessage.Length != 0 && false == prependMessage.EndsWith("\n"))
			{
				prependMessage += '\n';
			}

			WriteMessage(prependMessage + messageObject + appendMessage);
		}

		/// <summary>
		/// Writes Exception to the console, with optional messages before and after.
		/// </summary>
		/// <param name="exception">The Exception to write</param>
		/// <param name="appendMessage">message to put after the object</param>
		/// <param name="prependMessage">message to put before the object</param>
		public void WriteObject(Exception exception, string prependMessage = "", string appendMessage = "")
		{
			WriteObject(exception.ToString().Pastel(Color.Red), appendMessage, prependMessage);
		}



		#endregion IO


		// #####################################################################

		#region Internal Commands

		[InternalInteractiveCommand(commandName: "ListCommands", oneLineHelp: "Lists all commands there are.")]
		public void ListCommands([InteractiveCommandParameter("IncludeHidden", "Include hidden commands")]
			bool includeHidden = false)
		{
			WriteLine("All commands:");
			foreach (var kvp in mInvokeDict.ToArray().OrderBy(kvp => kvp.Value.Item2.CommandName))
			{
				var commandName = kvp.Value.Item2.CommandName;

				if (AliasToName.ContainsKey(kvp.Key)) continue;
				if (kvp.Value.Item2.Hidden && false == includeHidden) continue;

				WriteLine($"\t{commandName}: {kvp.Value.Item2.OneLineHelp}");
			}

			WriteLine();
		}

		[InternalInteractiveCommand(commandName: "Help",
			oneLineHelp: "Gives information about a command.",
			longHelp:
			"Provides detailed information about the command passed as parameter, including parameters, if any. \n" +
			"Note that some commands may not define all help fields, in that case they may be empty.\n" +
			"To see a list of available commands use: \"ListCommands\"")]
		public void Help([InteractiveCommandParameter("Command", "The command to display information for.")]
			string commandInput = null)
		{
			if (commandInput == null)
			{
				var attr = (InteractiveCommandAttribute)MethodBase.GetCurrentMethod()
					.GetCustomAttribute(typeof(InteractiveCommandAttribute));
				_ = attr ?? throw new Exception($"The {nameof(Help)} method should always have the {nameof(InteractiveCommandAttribute)} OR not get called with null.");
				Help(attr.CommandName);
				return;
			}

			var command = commandInput;
			if (ignoreCase)
				command = command.ToLowerInvariant();

			bool found = mInvokeDict.TryGetValue(command, out (MethodInfo, InteractiveCommandAttribute) result);
			if (found)
			{
				var attr = result.Item2;

				WriteLine($"{attr.CommandName}: {attr.OneLineHelp}");
				WriteLine(attr.LongHelp);
				if (attr.parameters != null && attr.parameters.Length > 0)
				{
					WriteLine($"Parameters of {attr.CommandName}:");
				}
				foreach (var parameter in attr.Parameters)
				{
					WriteLine($"  [#{parameter.ParameterPosition}] <{parameter.ParameterTypeName}> {parameter.ParameterName}: {parameter.ParameterOneLineHelp}");
					if (parameter.ParameterLongHelp != "")
					{
						WriteLine($"    {parameter.ParameterLongHelp}");
					}
					//TODO: Defaults / optional parameter handling.
				}

				if (attr.Aliases != null)
				{
					Write("Aliases: ");
					foreach (var ali in attr.Aliases)
					{
						Write($"{ali}, ");
					}
				}

				WriteLine();
			}
			else
			{
				WriteLine("Command not found, try \"ListCommands\" to find commands.");
				WriteLine($"Case sensitivity is {(ignoreCase ? "On" : "Off")}");
				WriteLine();
			}
		}

		[InternalInteractiveCommand(commandName: "Aliases", oneLineHelp: "Lists all commands and their Aliases.")]
		public void Aliases()
		{
			WriteLine("Aliases for all commands:");
			foreach (var kvp in mInvokeDict.ToArray().OrderBy(kvp => kvp.Key))
			{
				if (AliasToName.ContainsKey(kvp.Key)) continue;
				Write($"{kvp.Key}:");
				if (kvp.Value.Item2.Aliases == null)
				{
					WriteLine();
					continue;
				}

				foreach (var ali in kvp.Value.Item2.Aliases)
				{
					if (AliasToName.TryGetValue(ali, out string realName) && kvp.Key == realName)
					{
						Write($"\n\t{ali}");
					}
					else
					{
						// Alias was suppressed by another
					}
				}

				WriteLine();
			}

			WriteLine();
		}

		[InternalInteractiveCommand
			(oneLineHelp: "Hit a breakpoint just before executing the next Command."
		   , hidden: true)]
		public void DebugCommand()
		{
			DebugNextCommand = true;
			if (!Debugger.Launch())
			{
				WriteLine("Debugger declined to attach, breakpoint could not hit.");
			}
		}

		[InternalInteractiveCommand(commandName: "Quit",
			oneLineHelp: "Quits, will return to the session that started this one if applicable.",
			aliases: new string[] { "Exit" })]
		public void Quit()
		{
			run = false;
			WriteLine("Quitting...");
		}

		[InternalInteractiveCommand(oneLineHelp: "Get or Set if output coloring is enabled.")]
		public void ColorEnabled(bool? newValue = null)
		{
			if (newValue.HasValue)
			{
				Colors = newValue.Value;
			}

			WriteLine(Colors ? "Enabled" : "Disabled");
		}


		[InternalInteractiveCommand(oneLineHelp: "Reset all color and formatting.")]
		public void ResetFormatting()
		{
			Write("\x1b[0m");
		}

		[InternalInteractiveCommand(oneLineHelp: "Clear the screen.")]
		public void Clear()
		{
			ClearScreen();
		}


		#endregion

	}
}
