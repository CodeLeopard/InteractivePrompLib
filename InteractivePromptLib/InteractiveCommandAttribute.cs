﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;

using InteractivePromptLib.Reflection;

namespace InteractivePromptLib
{
	/// <summary>
	/// This attribute specifies that this method can be invoked from the program's main interactive prompt.
	/// If you want to create your own interactive prompt in a method you can inherit from this.
	/// Use the <see cref="InteractiveCommandParameterAttribute"/> to add information to the parameters (optional, though default will only provide name and type for parameters)
	/// </summary>
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	public class InteractiveCommandAttribute : Attribute
	{
		protected readonly string commandName;
		public readonly string OneLineHelp;
		public readonly string LongHelp;

		public readonly string[] Aliases;

		public readonly bool ShowReturnValue;

		public readonly bool Disabled;
		public readonly bool Hidden;

		internal MethodInfo method;
		internal InteractiveCommandParameter[] parameters;

		public string CommandName
		{
			get
			{
				if (string.IsNullOrWhiteSpace(commandName))
					return method.Name;
				else
					return commandName;
			}
		}

		public IReadOnlyList<InteractiveCommandParameter> Parameters => new ReadOnlyCollection<InteractiveCommandParameter>(parameters);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="commandNameOverride">Name of the command as typed into the console, if null it will take the name of the method.</param>
		/// <param name="oneLineHelp">Single line helptext</param>
		/// <param name="longHelp">Long helptext</param>
		/// <param name="aliases"></param>
		/// <param name="showReturnValue"></param>
		/// <param name="disabled">Disable this command, it'll be unavailable and unlisted.</param>
		/// <param name="hidden">Hide this command so it's not shown unless explicitly showing hidden commands.</param>
		public InteractiveCommandAttribute(
			string commandNameOverride = null,
			string oneLineHelp = null,
			string longHelp = null,
			string[] aliases = null,
			bool showReturnValue = true,
			bool disabled = false,
			bool hidden = false
			)
		{
			this.commandName = commandNameOverride;
			this.OneLineHelp = oneLineHelp;
			this.LongHelp = longHelp;

			this.Aliases = aliases;
			this.ShowReturnValue = showReturnValue;
			this.Disabled = disabled;
			this.Hidden = hidden;
		}

		internal void SetMethodInfo(MethodInfo mInfo)
		{
			if (null == method)
				method = mInfo;
			else
				throw new InvalidOperationException($"The {nameof(mInfo)} may be set only once.");


			SetupParameters();
		}

		private void SetupParameters()
		{
			if (null == method) throw new InvalidOperationException($"{nameof(method)} should not be null.");

			ParameterInfo[] parameterInfos = method.GetParameters();

			parameters = new InteractiveCommandParameter[parameterInfos.Length];

			foreach(ParameterInfo parameter in parameterInfos)
			{
				IEnumerable<Attribute> attributes = parameter.GetCustomAttributes();
				foreach(var attribute in attributes)
				{
					if (attribute is InteractiveCommandParameterAttribute parameterAttribute)
					{
						if (parameters[parameter.Position] == null)
						{
							var interactiveCommandParameter = new InteractiveCommandParameter(parameter, parameterAttribute);
							interactiveCommandParameter.parameterAttribute.SetParent(this);
							parameters[parameter.Position] = interactiveCommandParameter;
						}
						else
						{
							throw new ReflectionException($"Encountered duplicate attribute on parameter {parameter.Name} on method {method.DeclaringType}.{method.Name}.");
						}
					}
					//else continue;
				}
				if (parameters[parameter.Position] == null)
					parameters[parameter.Position] = new InteractiveCommandParameter(parameter, null);
			}
		}
	}
}
