using System.Linq;
using System.Reflection;

using InteractivePromptLib;

using Microsoft.VisualStudio.TestPlatform.TestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using static Tests.SharedExtensions;

namespace Tests
{
	[TestClass]
	public class A_Initialization
	{
		[TestMethod]
		public void A0_RawAttribute()
		{
			var myPrompt = new InteractivePrompt(
			                                 typeof(InteractiveCommandAttribute),
			                                 null,
			                                 "Program>",
			                                 searchAssemblies: Enumerable.Empty<Assembly>(),
			                                 searchClasses: Yield(typeof(Examples.RawAttribute.Lib1)));
		}

		[TestMethod]
		public void A1_CustomAttribute()
		{
			var myPrompt = new InteractivePrompt(
			                                     typeof(Examples.Basic.MyCommandAttribute),
			                                     null,
			                                     "Program>",
			                                     searchAssemblies: Enumerable.Empty<Assembly>(),
			                                     searchClasses: Yield(typeof(Examples.Basic.Lib1)));
		}
	}
}
