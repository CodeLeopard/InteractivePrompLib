﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;

using InteractivePromptLib.Reflection;

namespace InteractivePromptLib
{
	/// <summary>
	/// A class that exposes the properties of an object to console interaction.
	/// </summary>
	public class PropertyInteraction
	{
		private readonly Type attributeType;
		private readonly Type instanceType;

		protected object instance;



		protected readonly Dictionary<string, InteractablePropertyAttribute> myProperties = new Dictionary<string, InteractablePropertyAttribute>();
		protected readonly Dictionary<string, string> AliasToName = new Dictionary<string, string>();


		public readonly bool IgnoreCase;

		/// <summary>
		/// Get the value of the given name and print to console. If no name is supplied lists all gettable items.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="includeHidden">Show hidden items when listing.</param>
		public void Get(string name = null, bool includeHidden = false)
		{
			if (null == name)
			{
				Console.WriteLine("Gettable items: ");
				foreach (var kvp in myProperties.ToArray().OrderBy(kvp => kvp.Value.Name))
				{
					var itemName = kvp.Value.Name;

					if (AliasToName.ContainsKey(kvp.Key)) continue;
					if (kvp.Value.Hidden && false == includeHidden) continue;
					if (false == kvp.Value.IsGettable) continue;

					Console.WriteLine($"\t{itemName}: {kvp.Value.OneLineHelp}");
				}
				Console.WriteLine();
				return;
			}

			if (IgnoreCase) name = name.ToUpperInvariant();

			bool found = myProperties.TryGetValue(name, out InteractablePropertyAttribute attribute);

			if (false == found)
			{
				Console.WriteLine($"\"{name}\" not found.");
			}
			else
			{
				Console.WriteLine(attribute.GetValue(instance));
			}
		}



		/// <summary>
		/// Set the named item to the given value, which is Parsed if needed.
		/// </summary>
		/// <param name="input">Array of name, followed by words</param>
		public void Set(string[] input = null) // We must use string array because the interactivePrompt has already split the command.
		{
			if (null == input)
			{
				Console.WriteLine("Settable items:");
				foreach (var kvp in myProperties.ToArray().OrderBy(kvp => kvp.Value.Name))
				{
					var itemName = kvp.Value.Name;

					if (AliasToName.ContainsKey(kvp.Key)) continue;
					if (kvp.Value.Hidden /*&& false == includeHidden*/) continue;
					if (false == kvp.Value.IsSettable) continue;

					Console.WriteLine($"\t{itemName}: {kvp.Value.OneLineHelp}");
				}
				Console.WriteLine();
				return;
			}


			var name = input[0];
			if (IgnoreCase) name = name.ToUpperInvariant();

			var found = myProperties.TryGetValue(name, out InteractablePropertyAttribute attribute);
			if (false == found)
			{
				Console.WriteLine($"\"{name}\" not found.");
			}
			else
			{
				var member = attribute.Member;
				var data = input.Skip(1);
				StringBuilder sb = new StringBuilder(data.Sum((string s) => s.Length) + data.Count());

				foreach (string str in data)
				{
					sb.Append(str);
					sb.Append(' ');
				}

				sb.Remove(sb.Length - 1, 1);

				var originalData = sb.ToString();

				var valueType = attribute.ValueType;

				var success = ReflectionExt.TryParse(valueType, originalData, out dynamic newValue);

				if (success) attribute.SetValue(instance, newValue);
				else Console.WriteLine($"Failed to parse \"{originalData}\" into {attribute.ValueType.Name}");
			}
		}

		/// <summary>
		/// Initialize the interactor with a instance.
		/// </summary>
		/// <param name="attribute"></param>
		/// <param name="instance"></param>
		/// <param name="ignoreCase"></param>
		public PropertyInteraction(Type attribute, object instance, bool ignoreCase = true)
		{
			attributeType = attribute;
			this.instance = instance;
			this.instanceType = instance.GetType();
			this.IgnoreCase = ignoreCase;

			Initialize();
		}

		/// <summary>
		/// Initialize the interactor with a static Type.
		/// </summary>
		/// <param name="attribute"></param>
		/// <param name="staticType"></param>
		/// <param name="ignoreCase"></param>
		public PropertyInteraction(Type attribute, Type staticType, bool ignoreCase = true)
		{
			attributeType = attribute;
			this.instance = null;
			this.instanceType = staticType;
			this.IgnoreCase = ignoreCase;

			Initialize();
		}

		protected void Initialize()
		{
			if (null == attributeType) throw new InvalidOperationException($"{nameof(attributeType)} was null");
			if (! attributeType.IsAssignableFrom(typeof(InteractablePropertyAttribute)))
				throw new InvalidOperationException($"Parameter {nameof(attributeType)} should be assignable from the Type \"{nameof(InteractablePropertyAttribute)}\"");

			var results = new List<InteractablePropertyAttribute>();

			var found = ReflectionExt.FindMembersWithAttribute(instanceType, attributeType,
				BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);

			foreach (var (memberInfo, item2) in found)
			{
				var attribute = item2 as InteractablePropertyAttribute;

				if (null == attribute || attribute.Disabled) continue;

				attribute.SetMemberData(memberInfo);

				var name = attribute.Name;
				if (IgnoreCase)
					name = name.ToUpperInvariant();

				myProperties.Add(name, attribute);
				Debug.WriteLine($"Registered Interactable {attribute.Member.DeclaringType.Name}.{attribute.Member.Name} under name: {name}");

				results.Add(attribute);
			}

			foreach (var attribute in results)
			{
				if (attribute.Aliases != null)
				{
					foreach (var aliasCased in attribute.Aliases)
					{
						var alias = aliasCased;
						if (IgnoreCase) alias = aliasCased.ToUpperInvariant();

						if (myProperties.ContainsKey(alias))
						{
							Debug.WriteLine($"Unable to add alias:\"{alias}\" because that name already exists.");
						}
						else
						{
							AliasToName.Add(alias, attribute.Name);
							myProperties.Add(alias, attribute);
						}

					}
				}
			}
		}
	}
}
