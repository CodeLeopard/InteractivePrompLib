﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Globalization;

namespace InteractivePromptLib
{
	/// <summary>
	/// Information about the parameter of a command.
	/// </summary>
	public class InteractiveCommandNumberParameterAttribute : InteractiveCommandParameterAttribute
	{
		public readonly NumberStyles NumberStyles;


		public InteractiveCommandNumberParameterAttribute
			(NumberStyles numberStyles, string nameOverride = null, string oneLineHelp = null, string longHelp = null)
			: base(nameOverride: nameOverride, oneLineHelp: oneLineHelp, longHelp: longHelp)
		{
			NumberStyles = numberStyles;
		}
	}
}
