﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;

namespace InteractivePromptLib
{
	/// <summary>
	/// Attribute that tells the InteractivePrompt to search in this class for instances to call the method on.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false)]
	public class InteractiveCommandContainerAttribute : Attribute
	{
		public readonly Type commandAttribute;

		public InteractiveCommandContainerAttribute()
		{
			commandAttribute = typeof(InteractiveCommandAttribute);
		}

		protected InteractiveCommandContainerAttribute(Type commandAttribute)
		{
			if (! commandAttribute.IsAssignableFrom(typeof(InteractiveCommandAttribute)))
				throw new ArgumentException
					($"Should be assignable from {nameof(InteractiveCommandAttribute)}", nameof(commandAttribute));
			this.commandAttribute = commandAttribute;
		}
	}
}
